function initMap() {

	var $ = jQuery;
	var directionsService = new google.maps.DirectionsService;
	var polyField = $('[data-name="polyline"]').find('textarea')[0];

	$('[data-event="polyline-generate"]')[0].addEventListener('click', function (event) {
		generateCode(polyField);
	});

	$('[data-event="polyline-edit"]')[0].addEventListener('click', function (event) {
		lockEdit(polyField);
	});

	$('[data-event="polyline-delete"]')[0].addEventListener('click', function (event) {
		removePolyline(polyField);
	});

	function generateCode(polyField) {
		var startLat = $('[data-name="latitude"]').find('input')[0].value;
		var startLon = $('[data-name="longitude"]').find('input')[0].value;
		var endLat = $('[data-name="end_latitude"]').find('input')[0].value;
		var endLon = $('[data-name="end_longitude"]').find('input')[0].value;
		var wptLatArr = $('[data-name="waypoint_lat"]').find('input');
		var wptLonArr = $('[data-name="waypoint_lon"]').find('input');

		var start = startLat+','+startLon;
		var end = endLat+','+endLon;
		var waypoints = [];
		var wpt_count = 0;
		var waypointsCheck = true;

		// console.log($(wptLatArr));
		// console.log($(wptLonArr));

		if((wptLatArr.length > 1) || (wptLonArr.length > 1)) {
			wptLatArr.each(function() {
				if(!this.name.includes('acfcloneindex')) {
					if(waypointsCheck === true) {
						if(this.value !== '' && wptLonArr[wpt_count].value !== '') {

							point = this.value+','+wptLonArr[wpt_count].value;
							waypoints.push({
								location: point,
								stopover: true
							});

						} else {
							waypointsCheck = false;
						}
					}
				}				
				wpt_count++;
			});
		}

		if(!waypointsCheck) {
			alert('Latitude and Longitude must be entered for each Waypoint.');
			return;
		}

		if(startLat === '' || startLon === '' || endLat === '' || endLon === '') {
			alert('Latitude and Longitude must be entered for both the Start and the End of the path.');
			return;
		}

		var routeProps = {
			origin: start,
			destination: end,
			waypoints: waypoints,
			optimizeWaypoints: false,
			travelMode: 'BICYCLING'
		}

		
			directionsService.route(routeProps, function(response, status) {

				console.log('hello');
	
				var legs = response.routes[0].legs;
	
				polyField.value = JSON.stringify(response);
	
			});

	}

	function lockEdit(polyField) {
		var editButton = $('[data-event="polyline-edit"]')[0];

		if(polyField.readOnly) {
			polyField.readOnly = false;
			editButton.innerText = 'Lock';
		} else {
			polyField.readOnly = true;
			editButton.innerText = 'Edit';
		}
	}

	function removePolyline(polyField) {
		polyField.value = '';
	}
}
