		if(isset($field['value'])) {

		$vals = json_decode(esc_attr($field['value']));

			?>
			<div class="polyline-field-waypoints">
				<p>Waypoints</p>

				<div class="polyline-waypoints-table" id="polyline-waypoints-table">
					<div class="wapoints-table-row waypoints-table-header-row">
						<div class="waypoints-table-cell waypoints-header-num"></div>
						<div class="waypoints-table-cell waypoints-header-lat">Latitude</div>
						<div class="waypoints-table-cell waypoints-header-lon">Longitude</div>
						<div class="waypoints-table-cell waypoints-header-del"></div>
					</div>
						<?php if(isset($vals->waypoints) && is_object($vals->waypoints)) {
							$counter = 1;
							foreach($vals->waypoints as $key => $waypoint) {
								?>

					<div class="wapoints-table-row waypoints-table-body-row">
						<div class="waypoints-table-cell"><?php echo $counter; ?></div>
						<div class="waypoints-table-cell"><input type="text" name="<?php echo esc_attr($field['name']) ?>[waypoints][<?php echo $key; ?>][lat]" value="<?php echo esc_attr($waypoint->lat) ?>"></div>
						<div class="waypoints-table-cell"><input type="text" name="<?php echo esc_attr($field['name']) ?>[waypoints][<?php echo $key; ?>][lon]" value="<?php echo esc_attr($waypoint->lon) ?>"></div>
						<div class="waypoints-table-cell waypoints-del"></div>
					</div>

								<?php

								$counter++;
							}

						} else {

						?>

					<div class="wapoints-table-row">
						<span>No waypoints added</span>
					</div>


						<?php

						} ?>
				</div>

				<ul class="acf-actions acf-hl">
					<li>
						<a class="acf-button button button-primary polyline-table-add-row" href="#" data-event="add-row">Add Row</a>
					</li>
				</ul>

			</div>
			<textarea name="<?php echo esc_attr($field['name']) ?>[path]" rows="10" readonly><?php print_r($vals->path); ?></textarea>
			<div class="polyline_field_controls">
				<a class="acf-button button button-primary" href="#" data-event="polyline-generate">Generate</a>
				<a class="acf-button button" href="#" data-event="polyline-edit">Edit</a>
				<a class="acf-button button" href="#" data-event="polyline-delete">Remove</a>
			</div>
				<?php

			} else {

			?>
			<a class="acf-button button button-primary polyline-table-add-row" href="#" data-event="polyline-add" id="polyline-add-path">Add Path</a>
			<?php











					var table = $('#polyline-waypoints-table')[0];
		var pathButton = $('#polyline-add-path')[0];
		var field = pathButton !== null ? $(pathButton).parent().parent().attr('data-key') : $(table).parent().parent().parent().attr('data-key');

		// console.log(table);
		// console.log(field);

		$('.polyline-table-add-row').each(function(table, field) {
			this.addEventListener('click', function (event) {
				addRow(table, field);
			});
		});

		function addRow(table, field) {

			console.log($(table));

			var rows = $(table).find('.waypoints-table-body-row');

			$newRow =  '<div class="wapoints-table-row">';
			$newRow += '<div class="waypoints-table-cell">'+(rows.length+1)+'</div>';
			$newRow += '<div class="waypoints-table-cell"><input type="text" name="['+field+'][waypoints][wpt'+(rows.length+1)+'][lat]" value=""></div>';
			$newRow += '<div class="waypoints-table-cell"><input type="text" name="['+field+'][waypoints][wpt'+(rows.length+1)+'][lon]" value=""></div>';
			$newRow += '<div class="waypoints-table-cell waypoints-del"></div>';
			$newRow += '</div>';