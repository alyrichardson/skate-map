//*********** IMPORTS *****************
var gulp = require('gulp');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var rename = require('gulp-rename');
var map = require('map-stream');
var livereload = require('gulp-livereload');
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
global.errorMessage = '';

//Configuration
var sassFiles = [
	{
		watch: ['_src/sass/**/*.+(scss|sass)'],
		sass: '_src/sass/style.scss',
		output: '.',
		name: 'style.css',
	}
];
var jsFiles = [
	{
		watch: '_src/js/_jsfiles/**/*.js',
		output: 'assets/js/',
		name: 'main.js',
		nameMin: 'main.min.js'
	}
];
var phpFiles = [
	{
		watch: ['php/**/*.php', 'index.php']
	}
];
//END configuration

gulp.task('watch', function () {
	for (var i in sassFiles) {
		sassWatch(sassFiles[i]);
	}

	for (var j in jsFiles) {
		scriptWatch(jsFiles[j]);
	}

	// for (var k in phpFiles) {
	// 	fileWatch(phpFiles[k]);
	// }

	livereload.listen();
});

function sassWatch(sassData) {
	gulp.src(sassData.watch)
	.pipe(watch(sassData.watch, {emitOnGlob: true}, function() {
		gulp.src(sassData.sass)
		.pipe(sass(sassOptions))
		.on('error', function(err) {
				gutil.log(err.message);
				gutil.beep();
				global.errorMessage = err.message + " ";
		})
		.pipe(checkErrors())
		.pipe(rename(sassData.name))
		.pipe(autoprefixer({
			browsers: ['last 2 versions']
		}))
		.pipe(gulp.dest(sassData.output))
		.pipe(livereload());
	}));
}

function scriptWatch(jsData) {
	gulp.src(jsData.watch)
	.pipe(watch(jsData.watch, {emitOnGlob: true}, function() {
		gulp.src(jsData.watch)
		.pipe(concat(jsData.name))
		.pipe(gulp.dest(jsData.output))
		.pipe(uglify())
		.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
		.pipe(rename(jsData.nameMin))
		.pipe(gulp.dest(jsData.output))
		.pipe(livereload());
	}));
}

function fileWatch(fileData) {
	gulp.src(fileData.watch)
	.pipe(watch(fileData.watch, {emitOnGlob: true}, function() {
		gulp.src(fileData.watch)
		.pipe(livereload());
	}));
}

gulp.task('default', gulp.parallel('watch'));

/// Defaults yo
var sassOptions = {
	'outputStyle': 'compressed'
};

// Does pretty printing of sass errors
var checkErrors = function (obj) {
	function checkErrors(file, callback, errorMessage) {
		if (file.path.indexOf('.scss') != -1) {
				file.contents  = new Buffer("\
					body * { white-space:pre; }\
					body * { display: none!important; }\
					body:before {\
						white-space:pre;\
						content: '"+ global.errorMessage.replace(/(\\)/gm,"/").replace(/(\r\n|\n|\r)/gm,"\\A") +"';\
					}\
					html{background:#ccf!important; }\
				");
		}
		callback(null, file);
	}
	return map(checkErrors);
};

