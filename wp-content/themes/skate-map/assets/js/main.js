// Initialize global variables
var map, locations = [], markers = [], php_data, markerCluster, filteredMarkers, filterCheckbox, filterOptions, filterArrow, pathsVisible, distanceString, infoBox, activeLocation, initialZoom = 7;

// Main Google Map function
function initMap() {
	// Prepare data passed from PHP
	php_data.log_points.forEach(function(location) {
		location.position = new google.maps.LatLng(location.position_lat, location.position_lon);
		if(location.end_lat != null && location.end_lon != null) {
			location.destination = new google.maps.LatLng(location.end_lat, location.end_lon);
		} else {
			location.destination = null;
		}

		if(location.active) {
			initialZoom = 16;
		}
		locations.push(location);
	});

	var directionsService = new google.maps.DirectionsService;

	// Initialize Google Map
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: initialZoom,
		center: new google.maps.LatLng(php_data.center_lat, php_data.center_lon),
		mapTypeControlOptions: {
			mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map'],
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		}
	});
	
	// map.data.loadGeoJson('wp-content/themes/skate-map/assets/points.json');

	// Custom map Styles
	var paleDawnMap = new google.maps.StyledMapType(
	[
		{
			"featureType": "administrative",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"lightness": 33
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [
				{
					"color": "#f2e5d4"
				}
			]
		},
		{
			"featureType": "poi.park",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#c5dac6"
				}
			]
		},
		{
			"featureType": "poi.park",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"lightness": 20
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "all",
			"stylers": [
				{
					"lightness": 20
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#c5c6c6"
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "simplified"
				},
				{
					"weight": "1.00"
				},
				{
					"saturation": "-60"
				},
				{
					"lightness": "35"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#e4d7c6"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [
				{
					"saturation": "-60"
				},
				{
					"lightness": "35"
				}
			]
		},
		{
			"featureType": "road.local",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#fbfaf7"
				}
			]
		},
		{
			"featureType": "road.local",
			"elementType": "labels.icon",
			"stylers": [
				{
					"saturation": "-60"
				},
				{
					"lightness": "35"
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"color": "#acbcc9"
				}
			]
		}
	],
	{name: 'Pale Dawn'});

	// Associate the styled map with the MapTypeId and set it to display.
	map.mapTypes.set('styled_map', paleDawnMap);
	map.setMapTypeId('styled_map');

	// Create custom map controls
	var centerControlDiv = document.createElement('div');
	var centerControl = new CenterControl(centerControlDiv);
	centerControlDiv.className = 'custom_ui scrollable_ui';
	centerControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(centerControlDiv);

	// Create a pin for each location and add it to the map
	locations.forEach(function(feature) {
		var taxonomies = {
			venues: feature.venues,
			floors: feature.floors,
			events: feature.events
		};

		var marker = new google.maps.Marker({
			position: feature.position,
			map: map,
			title: feature.title,
			taxonomies: taxonomies
		});

		var infoPointer = '<div class="info_pointer"><div class="info_pointer_left"><div></div></div><div class="info_pointer_right"><div></div></div></div>';

		// Set up info box pop-ups
		feature.infoBoxString = infoPointer+
							'<div class="info_box_container"><div class="info_box_position">'+
							'<div class="feature_image" style="background-image: url(\''+feature.image+'\');"></div><div class="info_box_data">'+
							'<div class="info_box">'+
							'<div class="info_title">'+feature.title+'</div>';

		for(var taxonomy in php_data.terms) {
			var taxString = '';
			smallTax = taxonomy.toLowerCase();
			feature[smallTax].forEach(function(type) {
				taxString += (taxString == '') ? '<span class="info_cat">'+type+'</span>' : ', <span class="info_cat">'+type+'</span>';
			});
			feature.infoBoxString += '<div>'+taxonomy+' Type: '+taxString+'</div>';
		}

		map.addListener('click', function(event) {
			if(infoBox !== undefined) {
				infoBox.close();
				activeLocation = undefined;
			}
		});

		// If point is a trail, display directions polyline
		if(feature.polyline != null) {

			var directionsDisplay = new google.maps.DirectionsRenderer({
				hideRouteList: true,
				map: map,
				preserveViewport: true,
				suppressMarkers: true
			});

			feature.polyline.routes[0].overview_path.forEach(function(route, index, array) {
				array[index] = new google.maps.LatLng(this.lat, this.lng);
			});

			drawRoute(feature.polyline, marker, infoBox, directionsDisplay);
			distanceString = distanceCalc(feature.polyline, false);
			feature.infoBoxString += '<div class="distance">Distance: '+distanceString+'</div>';

		}
		feature.infoBoxString += '</div></div></div>';

		// Prepare marker clustering
		markers.push(marker);
		marker.addListener('click', function() {

			activeLocation = {marker: marker, contentString: feature.infoBoxString};
			openInfoBox(activeLocation);

		});

		if(feature.active === true) {
			activeLocation = {marker: marker, contentString: feature.infoBoxString};
		}
	});

	// Initialize marker clustering
	markerCluster = new MarkerClusterer(map, markers,
		{
			gridSize: 35,
			averageCenter: true
		}
	);

	// map.addListener('zoom_changed', function() {
	// 	if(map.getZoom() >= 11) {
	// 		markers.forEach(function(marker) {
	// 			marker.route.setMap(null);
	// 			marker.route.eventLine.forEach(function(segment) {
	// 				segment.setMap(null);
	// 			});
	// 		});
	// 	} else {
			
	// 	}
	// });
}

$( window ).on( 'load', function() {
	var boxText = document.createElement('div');
		boxText.className = 'info_box_position';
		boxText.innerHTML = '<div class="feature_image"><img src="" /></div>';

	var boxOptions = {
		alignBottom: true,
		closeBoxURL: '',
		content: boxText,
		disableAutoPan: true,
		enableEventPropagation: true,
		isHidden: false
	}

	if(infoBox === undefined) {
		infoBox = new InfoBox(boxOptions);
	}

	if(activeLocation !== undefined) {
		openInfoBox(activeLocation);
	}
});

// Filter dropdown
function CenterControl(controlDiv) {

	// Custom control border
	var filterButton = document.createElement('div');
	filterButton.className = 'custom_ui_button';
	filterButton.id = 'filter_button';

	// Custom control interior
	var filterUi = document.createElement('div');
	filterUi.innerHTML = 'Filter';
	filterUi.className = 'filter_ui hover_darken';

	// Filter arrow image
	filterArrow = document.createElement('img');
	filterArrow.className = 'arrow_img';
	filterArrow.src = 'https://maps.gstatic.com/mapfiles/arrow-down.png';

	// Filter checkbox
	filterCheckbox = document.createElement('input');
	filterCheckbox.type = 'checkbox';
	filterCheckbox.id = 'filter_checkbox';
	filterButton.title = 'Click to clear all filters';

	// Dropdown content
	filterOptDiv = document.createElement('div');
	filterOptDiv.className = 'filter_ui_container';
	filterOptions = document.createElement('ul');
	filterOptions.id = 'filter_options';
	filterOptions.className = 'filter_ui';
	filterOptDiv.appendChild(filterOptions);

	// Multi-dimensional list of terms
	for(var taxonomy in php_data.terms) {
		taxLi = document.createElement('li');
		taxUl = taxLi.appendChild(document.createElement('ul'));
		taxUl.className = 'filter_tax';
		taxUl.innerHTML = '<span class="filter_tax_name">'+taxonomy+'</span>';

		for(var term in php_data.terms[taxonomy]) {
			li = document.createElement('li');
			li.className = 'filter_term hover_darken';
			li.innerHTML = php_data.terms[taxonomy][term];

			li.addEventListener('click', function() {
				filterPins(this.innerHTML);
			});
			taxUl.appendChild(li);
		}
		filterOptions.appendChild(taxLi);
	};

	// Attach all to DOM
	filterUi.appendChild(filterArrow);
	filterUi.appendChild(filterCheckbox);
	filterButton.appendChild(filterUi);
	filterButton.appendChild(filterOptDiv);
	controlDiv.appendChild(filterButton);

	// Use term li elements to filter
	filterUi.addEventListener('click', function(e) {
		$(filterOptions).toggle();
	});

	filterCheckbox.addEventListener('change', function() {
		if(!this.checked) { filterPins('all', markers); }
	});
}

function drawRoute(response, marker, infowindow, directionsDisplay) {

	// Create route paths
	directionsDisplay.setDirections(response);
	directionsDisplay.eventLine = renderDirectionsPolylines(response, marker, infowindow);
	marker.route = directionsDisplay;

	// Bind map to marker's map, so that when a marker
	// is clustered, the path hides as well
	marker.route.bindTo('map', marker);
	marker.route.eventLine.forEach(function(segment) {
		segment.bindTo('map', marker);
	});

}

// Draw custom path that can be targeted with click events
function renderDirectionsPolylines(response, marker, infowindow) {
	var polylines = [];
	var polylineOptions = {
		strokeColor: '#2d49fe',
		strokeWeight: 7,
		strokeOpacity: 0,
		zIndex: 4
	};

	for (var i=0; i<polylines.length; i++) {
		polylines[i].setMap(null);
	}
	
	var legs = response.routes[0].legs;
	for (i = 0; i < legs.length; i++) {
		var steps = legs[i].steps;
		for (j = 0; j < steps.length; j++) {
			var nextSegment = steps[j].path;
			nextSegment.forEach(function(point, index, array) {
				array[index] = new google.maps.LatLng(point.lat, point.lng);
			});
			var stepPolyline = new google.maps.Polyline(polylineOptions);
			for (k = 0; k < nextSegment.length; k++) {
				stepPolyline.getPath().push(nextSegment[k]);
			}
			polylines.push(stepPolyline);
			stepPolyline.setMap(map);

			google.maps.event.addListener(stepPolyline, 'click', function(evt) {
				infowindow.open(map, marker);
				infoWindowSetup(markers);
			});

			google.maps.event.addListener(stepPolyline, 'mouseover', function() {
				polylines.forEach(function(segment) {
					segment.setOptions({strokeOpacity: 0.8});
				});
			});

			google.maps.event.addListener(stepPolyline, 'mouseout', function() {
				polylines.forEach(function(segment) {
					segment.setOptions({strokeOpacity: 0});
				});
			});
		}
	}
	polylines.taxonomies = marker.taxonomies;
	
	return polylines;
}

function distanceCalc(response, metric) {

	var totalDistance = 0;
	response.routes[0].legs.forEach(function(leg) {
		totalDistance += leg.distance.value;
	});

	if(!metric) {
		totalDistance = (totalDistance/1609.34).toFixed(2) + ' mi';
	} else {
		totalDistance = (totalDistance/1000).toFixed(2) + ' km';
	}

	return totalDistance;
}

function infoWindowSetup(boxDomHandle, markers) {

	var category_text = $(boxDomHandle).find('span.info_cat');

	for (var i = 0; i < category_text.length; i++) {
		category_text[i].addEventListener('click', function (event) {
			filterPins(event.target.innerText, markers);
		});
	}
}

// Filter pins on the map based on clicked keyword
function filterPins (keyword) {

	// Start with a clean slate
	markerCluster.clearMarkers();

	// Add back either all pins, or only the filtered ones
	if(keyword == ('all' || '')) {
		markers.forEach(function(marker) {
			markerCluster.addMarker(marker);
		});

		clearFilters();

	} else {
		markers.forEach(function(marker) {
			for(var filterTax in marker.taxonomies) {
				if (marker.taxonomies[filterTax] != null && marker.taxonomies[filterTax].includes(keyword)) {
					markerCluster.addMarker(marker);
				}
			}
		});

		filterTerms(keyword);
	}
}

// Style terms from list to show which is being used as a filter
function filterTerms (keyword) {
	termList = document.getElementsByClassName('filter_term');
	[].forEach.call(termList, function(li) {
		if(li.innerHTML == keyword) {
			$(li).addClass('checked');
		} else {
			$(li).removeClass('checked');
		}
	});
	filterCheckbox.checked = true;
	$('.filter_ui').addClass('filtered');
	$(filterCheckbox).show();
	$(filterOptions).hide();
}

// Remove all filters and 
function clearFilters () {
	termList = document.getElementsByClassName('filter_term');
	[].forEach.call(termList, function(li) {
		$(li).removeClass('checked');
	});
	filterCheckbox.checked = false;
	$('.filter_ui').removeClass('filtered');
	$(filterCheckbox).hide();
	$(filterOptions).hide();
}

function offsetCoords(latLng, offsetX, offsetY) {
	// https://stackoverflow.com/questions/10656743/how-to-offset-the-center-point-in-google-maps-api-v3

	var scale = Math.pow(2, map.getZoom());

	var worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latLng);
	var pixelOffset = new google.maps.Point((offsetX/scale) || 0,(offsetY/scale) ||0);

	var worldCoordinateNewCenter = new google.maps.Point(
		worldCoordinateCenter.x - pixelOffset.x,
		worldCoordinateCenter.y + pixelOffset.y
	);

	var newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

	return newCenter;
}

function openInfoBox(activeLocation) {

	marker = activeLocation.marker;
	contentString = activeLocation.contentString;

	var boxDomHandle = document.createElement('div');
	boxDomHandle.innerHTML = contentString;
	boxDomHandle.className = 'info_box_content';
	infoBox.setContent(boxDomHandle);

	infoBox.open(map, marker);
	infoBox.setPosition(marker.position);
	map.panTo(offsetCoords(marker.position, 0, -150));

	infoWindowSetup(boxDomHandle, markers);
}