// Initialize global variables
var map, locations = [], markers = [], php_data, markerCluster, filteredMarkers, filterCheckbox, filterOptions, filterArrow;

// Main Google Map function
function initMap() {
	// Prepare data passed from PHP
	php_data.log_points.forEach(function(location) {
		location.position = new google.maps.LatLng(location.position_lat, location.position_lon);
		locations.push(location);
	});

	// Initialize Google Map
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 7,
		center: new google.maps.LatLng(php_data.center_lat, php_data.center_lon),
		mapTypeControlOptions: {
			mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map'],
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		}
	});

	// Custom map Styles
	var paleDawnMap = new google.maps.StyledMapType(
	[
		{
			"featureType": "administrative",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"lightness": 33
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [
				{
					"color": "#f2e5d4"
				}
			]
		},
		{
			"featureType": "poi.park",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#c5dac6"
				}
			]
		},
		{
			"featureType": "poi.park",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"lightness": 20
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "all",
			"stylers": [
				{
					"lightness": 20
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#c5c6c6"
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "simplified"
				},
				{
					"weight": "1.00"
				},
				{
					"saturation": "-60"
				},
				{
					"lightness": "35"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#e4d7c6"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [
				{
					"saturation": "-60"
				},
				{
					"lightness": "35"
				}
			]
		},
		{
			"featureType": "road.local",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#fbfaf7"
				}
			]
		},
		{
			"featureType": "road.local",
			"elementType": "labels.icon",
			"stylers": [
				{
					"saturation": "-60"
				},
				{
					"lightness": "35"
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"color": "#acbcc9"
				}
			]
		}
	],
	{name: 'Pale Dawn'});

	// Associate the styled map with the MapTypeId and set it to display.
	map.mapTypes.set('styled_map', paleDawnMap);
	map.setMapTypeId('styled_map');

	// Create custom map controls
	var centerControlDiv = document.createElement('div');
	var centerControl = new CenterControl(centerControlDiv, map);
	centerControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(centerControlDiv);

	// Create a pin for each loaction and add it to the map
	locations.forEach(function(feature) {
		var taxonomies = {
			venues: feature.venues,
			floors: feature.floors,
			events: feature.events
		};

		var marker = new google.maps.Marker({
			position: feature.position,
			map: map,
			title: feature.title,
			taxonomies: taxonomies
		});

		// Set up info box pop-ups
		var contentString = '<div class="info_box">'+
							'<div class="info_title">'+feature.title+'</div>';

		for(var taxonomy in php_data.terms) {
			var taxString = '';
			smallTax = taxonomy.toLowerCase();
			feature[smallTax].forEach(function(type) {
				taxString += (taxString == '') ? '<span class="info_cat">'+type+'</span>' : ', <span class="info_cat">'+type+'</span>';
			});
			contentString += '<div>'+taxonomy+' Type: '+taxString+'</div>';
		}

		contentString += '</div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});


		// Prepare marker clustering
		markers.push(marker); 
		marker.addListener('click', function() {
			infowindow.open(map, marker);

			// Use text from taxonomies to filter
			var category_text = document.getElementsByClassName('info_cat');

			for (var i = 0; i < category_text.length; i++) {
				category_text[i].addEventListener('click', function (event) {
					filterPins(event.target.innerText, markers);
				});
			}
		});
	});

	// Initialize marker clustering
	markerCluster = new MarkerClusterer(map, markers,
		{
			gridSize: 35,
			averageCenter: true
		}
	);
}

// Filter dropdown
function CenterControl(controlDiv, map) {

	// Custom control border
	var filterButton = document.createElement('div');
	filterButton.id = 'filter_button';

	// Custom control interior
	var filterUi = document.createElement('div');
	filterUi.innerHTML = 'Filter';
	filterUi.className = 'filter_ui';

	// Filter arrow image
	filterArrow = document.createElement('img');
	filterArrow.src = 'https://maps.gstatic.com/mapfiles/arrow-down.png';

	// Filter checkbox
	filterCheckbox = document.createElement('input');
	filterCheckbox.type = 'checkbox';
	filterCheckbox.id = 'filter_checkbox';
	filterButton.title = 'Click to clear all filters';

	// Dropdown content
	filterOptions = document.createElement('ul');
	filterOptions.id = 'filter_options';
	filterOptions.className = 'filter_ui';

	// Multi-dimensional list of terms
	for(var taxonomy in php_data.terms) {
		taxLi = document.createElement('li');
		taxUl = taxLi.appendChild(document.createElement('ul'));
		taxUl.className = 'filter_tax';
		taxUl.innerHTML = '<span class="filter_tax_name">'+taxonomy+'</span>';

		for(var term in php_data.terms[taxonomy]) {
			li = document.createElement('li');
			li.className = 'filter_term';
			li.innerHTML = php_data.terms[taxonomy][term];

			li.addEventListener('click', function() {
				filterPins(this.innerHTML);
			});
			taxUl.appendChild(li);
		}
		filterOptions.appendChild(taxLi);
	};

	// Attach all to DOM
	filterUi.appendChild(filterArrow);
	filterUi.appendChild(filterCheckbox);
	filterButton.appendChild(filterUi);
	filterButton.appendChild(filterOptions);
	controlDiv.appendChild(filterButton);

	// Use term li elements to filter
	filterUi.addEventListener('click', function(e) {
		$(filterOptions).toggle();
	});

	filterCheckbox.addEventListener('change', function() {
		if(!this.checked) { filterPins('all', markers); }
	});
}

// Filter pins on the map based on clicked keyword
function filterPins (keyword) {

	// Start with a clean slate
	markerCluster.clearMarkers();

	// Add back either all pins, or only the filtered ones
	if(keyword == ('all' || '')) {
		markers.forEach(function(marker) {
			markerCluster.addMarker(marker);
		});

		clearFilters();

	} else {
		markers.forEach(function(marker) {
			for(var filterTax in marker.taxonomies) {
				if (marker.taxonomies[filterTax] != null && marker.taxonomies[filterTax].includes(keyword)) {
					markerCluster.addMarker(marker);
				}
			};
		});

		filterTerms(keyword);
	}
}

// Style terms from list to show which is being used as a filter
function filterTerms (keyword) {
	termList = document.getElementsByClassName('filter_term');
	[].forEach.call(termList, function(li) {
		if(li.innerHTML == keyword) {
			$(li).addClass('checked');
		} else {
			$(li).removeClass('checked');
		}
	});
	filterCheckbox.checked = true;
	$(filterCheckbox).show();
	$(filterOptions).hide();
}

// Remove all filters and 
function clearFilters () {
	termList = document.getElementsByClassName('filter_term');
	[].forEach.call(termList, function(li) {
		$(li).removeClass('checked');
	});
	filterCheckbox.checked = false;
	$(filterCheckbox).hide();
	$(filterOptions).hide();
}