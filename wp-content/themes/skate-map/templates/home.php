<?php
/*
Template Name: Home
*/
?>
<?php get_header();

	$news_id = get_field('homepage_news');
	$event_id = get_field('homepage_event');
	$som_id = get_field('homepage_som');

	if($news_id == '') {
		$args = array(
			'category_name' => 'news',
			'posts_per_page' => 1
		);
	} else {
		$args = array(
			'p' => $news_id
		);
	}
	$news_post = new WP_Query($args);
	if($news_post->have_posts()) { 
		while ( $news_post->have_posts() ) {

			$news_post->the_post();
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'category_thumbnail');

			$news_tiles[] = '<li style="background-image: url(\'' . $thumb[0] . '\');"><a href="' . get_permalink() . '"><div class="tile_title"><span>Latest News</span></div></a></li>';
		}
	}
	wp_reset_postdata(); 

	if($event_id == '') {
		$args = array (
			'post_type' => 'post',
			'category_name' => 'events',
			'order' => 'ASC',
			'posts_per_page' => 1,
			'meta_query' => array(
				array(
					'key'		=> 'event_date',
					'compare'	=> '>=',
					'value'		=> date('Ymd')
				)
			),
		);
	} else {
		$args = array(
			'p' => $event_id
		);
	}
	$event_post = new WP_Query($args);
	if($event_post->have_posts()) { 
		while ( $event_post->have_posts() ) {

			$event_post->the_post();
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'category_thumbnail');

			$news_tiles[] = '<li style="background-image: url(\'' . $thumb[0] . '\');"><a href="' . get_field('facebook_link') . '" target="_blank"></a></li>';
		}
	} else {
		$news_tiles[] = '<li style="background: none; display: flex; align-items: center; justify-content: center;">No Upcoming Events</li>';
	}
	wp_reset_postdata();

	if($som_id == '') {
		$args = array(
			'category_name' => 'skater-of-the-month',
			'posts_per_page' => 1
		);
	} else {
		$args = array(
			'p' => $som_id
		);
	}
	$category_posts = new WP_Query($args);
	if($category_posts->have_posts()) { 
		while ( $category_posts->have_posts() ) { 

			$category_posts->the_post();
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'category_thumbnail');

			$news_tiles[] = '<li style="background-image: url(\'' . $thumb[0] . '\');"><a href="' . get_permalink() . '"><div class="tile_title"><span>Skater of the Month</span></div></a></li>';
		}
	}
	wp_reset_postdata();

	$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
	$page_query = new WP_Query( array( 'paged' => $paged ) );
	if($page_query->have_posts()) { 
		while ( $page_query->have_posts() ){ $page_query->the_post();

			if( have_rows('calls_to_action') ) {
				while ( have_rows('calls_to_action') ) { the_row();
					$cta_tiles[] = '<a class="cta_button" href="' . get_sub_field('url') . '"><img src="' . get_sub_field('image') . '" title="' . get_sub_field('title') . '" /></a>';
				}
			}
		}
	}
	wp_reset_postdata();
	?>

	<div class="news_tiles">
		<ul>

	<?php

	foreach($news_tiles as $link) {
		echo $link;
	} ?>

		</ul>
	</div>

	<div class="cta_tiles">
		<ul>

	<?php

	foreach($cta_tiles as $link) {
		echo '<li>' . $link . '</li>';
	} ?>

		</ul>
	</div>
</div>

<?php get_footer(); ?>