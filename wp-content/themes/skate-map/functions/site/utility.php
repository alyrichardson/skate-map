<?php

function __set_all_posts_for_author( $query ) {
	if (is_admin() && is_post_type_archive(array('log_point') ) ) {
		$current_user = wp_get_current_user();
		$query->set( 'author', $current_user->ID );
	}
}
if(!current_user_can( 'edit_others_posts' )) {
	add_action( 'pre_get_posts', '__set_all_posts_for_author' );
}

function remove_menus(){

	if(!current_user_can( 'edit_others_posts' )) {
		remove_menu_page( 'index.php' );                  //Dashboard
		remove_menu_page( 'jetpack' );                    //Jetpack* 
		remove_menu_page( 'edit.php' );                   //Posts
		remove_menu_page( 'upload.php' );                 //Media
		remove_menu_page( 'edit.php?post_type=page' );    //Pages
		remove_menu_page( 'edit-comments.php' );          //Comments
		remove_menu_page( 'themes.php' );                 //Appearance
		remove_menu_page( 'plugins.php' );                //Plugins
		remove_menu_page( 'users.php' );                  //Users
		remove_menu_page( 'tools.php' );                  //Tools
		remove_menu_page( 'options-general.php' );        //Settings
	}
}
add_action( 'admin_menu', 'remove_menus' );

function print_array($array) {
	?><pre><?php
	print_r($array);
	?></pre><?php
}
