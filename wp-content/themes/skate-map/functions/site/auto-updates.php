<?php

// If I'm not around, I don't trust the league to remember to update the site. I would rather have it break and need a little fixing, than have it hacked. -Aly

add_filter( 'allow_minor_auto_core_updates', '__return_true' ); // Enable minor updates
add_filter( 'allow_major_auto_core_updates', '__return_true' ); // Enable major updates
add_filter( 'auto_update_plugin', '__return_true' ); // Enable plugin updates