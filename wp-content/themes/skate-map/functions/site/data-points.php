<?php

	global $lat_average_value, $lon_average_value, $lat_lon_counter, $waypoints;

	/*  To make a new map for someone, add them as a WP user and create a new page
		with a title that matches their user name. This logic outputs different posts
		depending on which user's page we want to see. Front page is default user. */

	if( get_user_by('slug', $post->post_name) !== false ) {

		$user = $post->post_name;

	} else if( is_single() ) {

		$user = get_the_author_meta('user_nicename', $post->post_author);

	} else {

		$user = get_userdata(1)->user_login;
	
	}

	$active_post = ( is_single() ) ? $post->post_name : '';

	$data = array(
		'zoom_level' => 11,
		'log_points' => Array(),
		'terms' 	 => Array( // Get all available terms, for filtering
			'Venues' => get_terms([
				'taxonomy' => 'venue_type',
				'hide_empty' => false,
				'fields' => 'names'
			]),
			'Floors' => get_terms([
				'taxonomy' => 'floor_type',
				'hide_empty' => false,
				'fields' => 'names'
			]),
			'Events' => get_terms([
				'taxonomy' => 'event_type',
				'hide_empty' => false,
				'fields' => 'names'
			])
		)
	);

	$args = array(
		'author_name' => $user,
		'post_type' => 'log_point',
		'posts_per_page' => -1
	);

	// Set up data for each location
	$log_points = new WP_Query($args);
	if($log_points->have_posts()) { 
		while ( $log_points->have_posts() ) {
			$log_points->the_post();

			if( $post->post_name === $active_post ) {
				$data['center_lat'] = get_field('latitude');
				$data['center_lon'] = get_field('longitude');

				$location['active'] = true;
			}

			if( get_field('waypoints') ) {
				foreach(get_field('waypoints') as $point) {
					$waypoints[]['location'] = $point['waypoint_lat'] . ',' . $point['waypoint_lon'];
				}
			}

			$location['title'] 			= html_entity_decode( $post->post_title );
			$location['slug'] 			= $post->post_name;
			$location['image']			= has_post_thumbnail() ? get_the_post_thumbnail_url( null, 'marker_image' ) : '/skate-map/wp-content/uploads/2017/12/no-photo.jpg';
			$location['position_lat'] 	= get_field('latitude');
			$location['position_lon'] 	= get_field('longitude');
			$location['end_lat'] 		= get_field('end_latitude');
			$location['end_lon'] 		= get_field('end_longitude');
			$location['waypoints'] 		= isset( $waypoints) ? $waypoints : null;
			$location['polyline'] 		= get_field('polyline');
			$location['venues'] 		= wp_get_post_terms( $id, 'venue_type', array('fields' => 'names') );
			$location['floors'] 		= wp_get_post_terms( $id, 'floor_type', array('fields' => 'names') );
			$location['events'] 		= wp_get_post_terms( $id, 'event_type', array('fields' => 'names') );
			$location['active']			= isset( $location['active'] ) ? $location['active'] : false;

			$lat_average_value += $location['position_lat'];
			$lon_average_value += $location['position_lon'];
			$lat_lon_counter++;

			$location['end_lat'] = $location['end_lat'] == '' ? null : $location['end_lat'];
			$location['end_lon'] = $location['end_lon'] == '' ? null : $location['end_lon'];
			$location['polyline'] = $location['polyline'] == '' ? null : json_decode($location['polyline']);

			unset($waypoints);

			$data['log_points'][] = $location;
			unset($location);
		}
	}

	// Get the average latitude/longitude of all points and use the result as the center zoom.
	if (!isset($data['center_lat']) || !isset($data['center_lon'])) {
		$data['center_lat'] = ( $lat_average_value !== null && $lat_lon_counter !== null ) ? $lat_average_value / $lat_lon_counter : null;
		$data['center_lon'] = ( $lon_average_value !== null && $lat_lon_counter !== null ) ? $lon_average_value / $lat_lon_counter : null;
	}

	// Package up Wordpress data and pass it to JS script
	wp_localize_script( 'script-main', 'php_data', $data );
	