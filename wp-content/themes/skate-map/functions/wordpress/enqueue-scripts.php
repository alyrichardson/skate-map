<?php

function enqueue_custom_scripts() {

	// Add main stylesheet
	wp_register_style('style-main', (get_stylesheet_directory_uri() . '/style.css'), false, false, 'screen');
	wp_enqueue_style('style-main');

	// Remove default jQuery and add Google hosted version to the footer
	wp_deregister_script('jquery-core');
	wp_register_script('jquery-core', ('//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'), false, '2.2.4', true);
	wp_enqueue_script('jquery-core');

	wp_register_script('gmap-clusterer', (get_stylesheet_directory_uri() . '/assets/js/vendor/gmaps-marker-clusterer-master/src/markerclusterer.js'));
	wp_enqueue_script('gmap-clusterer');

	wp_register_script('script-main', (get_stylesheet_directory_uri() . '/assets/js/main.js'), array('gmap-clusterer'), '1.0.0', true);
	wp_enqueue_script('script-main');

	wp_register_script('gmaps', ('https://maps.googleapis.com/maps/api/js?key=AIzaSyCl00CJOMauYhlMeEF7-PbHus6ygaOVLYA&callback=initMap'), array(), '', true);
	wp_enqueue_script('gmaps');

	wp_register_script('gmaps-infobox', (get_stylesheet_directory_uri() . '/assets/js/vendor/infobox/src/infobox.js'), array('gmaps'), '', true);
	wp_enqueue_script('gmaps-infobox');	

}
add_action( 'wp_enqueue_scripts', 'enqueue_custom_scripts' );