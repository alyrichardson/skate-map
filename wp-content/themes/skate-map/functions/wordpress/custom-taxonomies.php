<?php
// http://codex.wordpress.org/Function_Reference/register_taxonomy
function register_custom_taxonomies() {

	// --------------------------------
	// Taxonomy Name
	// --------------------------------

	register_taxonomy(
		'venue_type', // taxonomy ID. Make this unique from CPTs and Pages to avoid URL rewrite headaches.
		array(
			'log_point' // applicable post type
		),
		array(
			'hierarchical' => true,
			'show_ui' => true,
			'public' => true,
			'label' => __('Venue Type'),
			'show_in_nav_menus' => false,
			'labels' => array(
				'add_new_item' => 'Add New Venue Type'
			),
			'query_var' => true,
		)
	);

	register_taxonomy(
		'floor_type', // taxonomy ID. Make this unique from CPTs and Pages to avoid URL rewrite headaches.
		array(
			'log_point' // applicable post type
		),
		array(
			'hierarchical' => true,
			'show_ui' => true,
			'public' => true,
			'label' => __('Floor Type'),
			'show_in_nav_menus' => false,
			'labels' => array(
				'add_new_item' => 'Add New Floor Type'
			),
			'query_var' => true,
		)
	);

	register_taxonomy(
		'event_type', // taxonomy ID. Make this unique from CPTs and Pages to avoid URL rewrite headaches.
		array(
			'log_point' // applicable post type
		),
		array(
			'hierarchical' => true,
			'show_ui' => true,
			'public' => true,
			'label' => __('Event Type'),
			'show_in_nav_menus' => false,
			'labels' => array(
				'add_new_item' => 'Add New Event Type'
			),
			'query_var' => true,
		)
	);
}
add_action( 'init', 'register_custom_taxonomies' );
