<?php

// --------------------------------
// Register Custom Image Crops
// --------------------------------

if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'marker_image', 500, 375, array( 'center', 'top' ) );
}
