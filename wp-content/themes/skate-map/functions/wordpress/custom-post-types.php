<?php
// http://codex.wordpress.org/Function_Reference/register_post_type
function register_custom_post_types() {

	// --------------------------------
	// CPT Name
	// --------------------------------

	$labels = array(
		'name' => _x('Skate Log', 'post type general name'),
		'singular_name' => _x('Log Point', 'post type singular name'),
		'add_new' => _x('Add New', 'log-point'),
		'add_new_item' => __('Add New Point'),
		'edit_item' => __('Edit Point'),
		'new_item' => __('New Point'),
		'view_item' => __('View Point'),
		'search_items' => __('Search Log Points'),
		'not_found' =>  __('No Log Points found'),
		'not_found_in_trash' => __('No Log Points found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Skate Log'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'query_var' => true,
		'rewrite' => array('slug' => 'log-point'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 29,
		'menu_icon' => 'dashicons-sticky',
		'supports' => array('title', 'author', 'thumbnail', 'page-attributes')
	);
	register_post_type('log_point', $args);

}
add_action( 'init', 'register_custom_post_types' );
