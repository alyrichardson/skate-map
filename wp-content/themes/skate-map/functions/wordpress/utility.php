<?php

// --------------------------------
// Required
// --------------------------------

// Forcing IE Compatibility Mode
header('X-UA-Compatible: IE=Edge');

// Clean up WP Header
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

// Add Wordpress Feature Support
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5' );

//---------------------------------
// Optional
//---------------------------------

// Hide WP Admin Bar
add_filter( 'show_admin_bar', '__return_false' );